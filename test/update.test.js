const TestComponent = require('./TestComponent');

const node = document.createElement('div');
const props = {
  name: 'Ann'
};
const nextProps = {
  name: 'Glen',
  city: 'Kharkov'
};

describe('Test cases for update lifecycle', () => {
  const inst = new TestComponent(props, node);

  describe('Test cases for methods', () => {
    Object.values(inst.mocks).forEach(mockFn => mockFn.mockClear());
    inst.props = nextProps;

    test('1) componentWillReceiveProps should provide 1 arguments', () => {
      expect(inst.mocks.componentWillReceiveProps.mock.calls[0]).toHaveLength(1);
    });
    test('1.1) componentWillReceiveProps should take next props as first argument', () => {
      expect(inst.mocks.componentWillReceiveProps).toHaveBeenCalledWith(nextProps);
    });
    test('1.2) componentWillReceiveProps should have old props in this', () => {
      expect(inst.mocks.componentWillReceiveProps).toReturnWith(props);
    });
    test('2) componentShouldUpdate should take 1 arguments', () => {
      expect(inst.mocks.componentShouldUpdate.mock.calls[0]).toHaveLength(1);
    });
    test('2.1) componentShouldUpdate should take next props as first argument', () => {
      expect(inst.mocks.componentShouldUpdate).toHaveBeenCalledWith(nextProps);
    });
    test('3) componentWillUpdate should provide 0 arguments', () => {
      expect(inst.mocks.componentWillUpdate.mock.calls[0]).toHaveLength(0);
    });
    test('3.1) componentWillUpdate should have new props in this', () => {
      expect(inst.mocks.componentWillUpdate).toReturnWith(nextProps);
    });

    test('4) componentDidUpdate should provide 1 arguments', () => {
      expect(inst.mocks.componentDidUpdate.mock.calls[0]).toHaveLength(1);
    });
    test('4.1) componentDidUpdate should take previous props as first argument', () => {
      expect(inst.mocks.componentDidUpdate).toHaveBeenCalledWith(props);
    });
    test('4.2) componentDidUpdate should have new props in this', () => {
      expect(inst.mocks.componentDidUpdate).toReturnWith(nextProps);
    });
  });

  describe('Test cases for order in lifecycle by default', () => {
    test('1) order in lifecycle by default', () => {
      Object.values(inst.mocks).forEach(mockFn => mockFn.mockClear());
      inst.props = nextProps;

      expect(inst.mocks.componentWillReceiveProps).toHaveBeenCalledTimes(1);
      expect(inst.mocks.componentShouldUpdate).toHaveBeenCalledTimes(1);
      expect(inst.mocks.componentWillUpdate).toHaveBeenCalledTimes(1);
      expect(inst.mocks.render).toHaveBeenCalledTimes(1);
      expect(inst.mocks.componentDidUpdate).toHaveBeenCalledTimes(1);

      expect(inst.mocks.componentWillMount).not.toHaveBeenCalled();
      expect(inst.mocks.componentWillUnmount).not.toHaveBeenCalled();
      expect(inst.mocks.componentDidMount).not.toHaveBeenCalled();

      expect(inst.mocks.componentWillReceiveProps.mock.invocationCallOrder[0])
        .toBeLessThan(inst.mocks.componentShouldUpdate.mock.invocationCallOrder[0]);
      expect(inst.mocks.componentShouldUpdate.mock.invocationCallOrder[0])
        .toBeLessThan(inst.mocks.componentWillUpdate.mock.invocationCallOrder[0]);
      expect(inst.mocks.componentWillUpdate.mock.invocationCallOrder[0])
        .toBeLessThan(inst.mocks.render.mock.invocationCallOrder[0]);
      expect(inst.mocks.render.mock.invocationCallOrder[0])
        .toBeLessThan(inst.mocks.componentDidUpdate.mock.invocationCallOrder[0]);
    });
  });

  describe('Test cases for order in lifecycle with false in componentShouldUpdate', () => {
    test('1) order in lifecycle with false in componentShouldUpdate', () => {
      Object.values(inst.mocks).forEach(mockFn => mockFn.mockClear());

      inst.mocks.componentShouldUpdate.mockImplementation(() => false);
      inst.props = nextProps;

      expect(inst.mocks.componentWillReceiveProps).toHaveBeenCalledTimes(1);
      expect(inst.mocks.componentShouldUpdate).toHaveBeenCalledTimes(1);

      expect(inst.mocks.componentWillUpdate).not.toHaveBeenCalled();
      expect(inst.mocks.render).not.toHaveBeenCalled();
      expect(inst.mocks.componentDidUpdate).not.toHaveBeenCalled();
      expect(inst.mocks.componentWillMount).not.toHaveBeenCalled();
      expect(inst.mocks.componentDidMount).not.toHaveBeenCalled();
      expect(inst.mocks.componentWillUnmount).not.toHaveBeenCalled();

      expect(inst.mocks.componentWillReceiveProps.mock.invocationCallOrder[0])
        .toBeLessThan(inst.mocks.componentShouldUpdate.mock.invocationCallOrder[0]);
    });
  });
});
